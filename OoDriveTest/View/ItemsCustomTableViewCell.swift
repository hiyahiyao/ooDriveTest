//
//  ItemsCustomTableViewCell.swift
//  OoDriveTest
//
//  Created by Pierre Yao on 22/04/2016.
//  Copyright © 2016 Pierre Yao. All rights reserved.
//

import UIKit

class ItemsCustomTableViewCell: UITableViewCell {
    
    @IBOutlet var name : UILabel!
    @IBOutlet var type : UILabel!
    @IBOutlet var indicator : UIActivityIndicatorView!
    @IBOutlet var illustration : UIImageView!
    
    var cellItem : Item!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    /**
     Assign an item to a cell
     @param item : an item
     */
    
    func setItem(item : Item) {
        cellItem = item
        name.text = cellItem.name
        type.text = cellItem.contentType
        
        if (cellItem.isDir == true) {
            illustration.image = UIImage(named: "folder_img")
            illustration.tintColor = generalBlueColor
        }
        else {
            startActivity()
            if cellItem.itemFileType != nil {
                switch cellItem.itemFileType! {
                case FileType.Image:
                    if (item.filePath != nil) {
                        let image = UIImage(contentsOfFile: item.filePath!)
                        illustration.image = image
                        hideActivity()
                    }
                    else {
                        illustration.image = UIImage(named: "image_img")
                    }
                case FileType.PDF:
                    print("it's a pdf file")
                    illustration.image = UIImage(named: "pdf_img")
                    hideActivity()
                case FileType.Audio:
                    print("it's an audio file")
                    illustration.image = UIImage(named: "audio_img")
                    hideActivity()
                case FileType.Video:
                    print("it's a video file")
                    illustration.image = UIImage(named: "video_img")
                    hideActivity()
                default:
                    illustration.image = UIImage(named: "file_img")
                    hideActivity()
                }
            }
            else {
                illustration.image = UIImage(named: "file_img")
                hideActivity()
            }
        }
    }
    
    /**
     Start an activity indicator
     */
    
    func startActivity() {
        indicator.hidden = false
        indicator.startAnimating()
    }
    
    /**
     Hide an activity indicator
     */
    
    func hideActivity() {
        indicator.hidden = true
        indicator.stopAnimating()
    }
    
    /*func updateCell() {
     //let image = UIImage(contentsOfFile: ApplicationManager.sharedInstance.requester.loadFile(cellItem.id)) {
     let image = UIImage(contentsOfFile: ApplicationManager.sharedInstance.requester.loadFile(cellItem.id))
     illustration.image = image
     }*/
    
}
