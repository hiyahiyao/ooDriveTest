//
//  AccountViewController.swift
//  OoDriveTest
//
//  Created by Pierre Yao on 24/04/2016.
//  Copyright © 2016 Pierre Yao. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {
    
    @IBOutlet var userImage : UIImageView!
    @IBOutlet var lastName : UILabel!
    @IBOutlet var firstName : UILabel!
    @IBOutlet var downloadCount : UILabel!
    @IBOutlet var tLastName : UILabel!
    @IBOutlet var tFirstName : UILabel!
    @IBOutlet var tDownloadCount : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userImage.layer.borderWidth = 3
        self.userImage.layer.borderColor = UIColor.whiteColor().CGColor

        let user = ApplicationManager.sharedInstance.user
        lastName.text = user.lastName
        firstName.text = user.firstName
        
        let files = ApplicationManager.sharedInstance.requester.listFile()
        downloadCount.text = String(files.count) + " Fichiers téléchargés"
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : - Action
    
    @IBAction func showGallerie() {
        // a gallerie that present all pictures download in a slideshow
    }
    
    @IBAction func close() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
