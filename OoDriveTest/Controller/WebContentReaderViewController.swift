//
//  WebContentReaderViewController.swift
//  OoDriveTest
//
//  Created by Pierre Yao on 24/04/2016.
//  Copyright © 2016 Pierre Yao. All rights reserved.
//

import UIKit

class WebContentReaderViewController: ReaderViewController, UIWebViewDelegate {

    @IBOutlet var webview : UIWebView!
    @IBOutlet var indicatorActivity : activityIndicator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = fileName
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        webview.delegate = self
        webview.hidden = true
        indicatorActivity.strokeColor = UIColor.whiteColor()
        
        let url = NSURL(fileURLWithPath: filePath)
        //let url = NSURL(string: "http://google.com")
        let request : NSURLRequest = NSURLRequest(URL: url)
        webview.loadRequest(request)
        
        showIndicator()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : - Webview delegate method
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        webview.hidden = false
        hideIndicator()
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        webview.hidden = false
        hideIndicator()
    }
    
    /**
     Start an activity indicator
     */
    
    func hideIndicator() {
        indicatorActivity.completeLoading(true)
        indicatorActivity.alpha = 1
        UIView.animateWithDuration(0.2) {
            self.indicatorActivity.alpha = 0
        }
    }
    
    /**
     Hide an activity indicator
     */
    
    func showIndicator() {
        indicatorActivity.alpha = 0
        UIView.animateWithDuration(0.2) {
            self.indicatorActivity.alpha = 1
        }
        indicatorActivity.startLoading()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
