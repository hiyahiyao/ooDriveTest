//
//  HomeViewController.swift
//  OoDriveTest
//
//  Created by Pierre Yao on 22/04/2016.
//  Copyright © 2016 Pierre Yao. All rights reserved.
//

import UIKit
import Photos
import AVKit
import AVFoundation

let IdentifierItemCell = "IdentifierItemCell"

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var tableView : UITableView!
    @IBOutlet var back : UIBarButtonItem!
    @IBOutlet var emptyTableViewInfo : UILabel!
    @IBOutlet var indicator : activityIndicator!
    @IBOutlet var maskView : UIView!

    var videoPlayer : AVPlayer!
    var Audioplayer = AVAudioPlayer()
    
    var itemsArray : [Item]!
    var arborescence : [String]!
    var currentRootItemId : String!
    var firstFetch : Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "OoTest"
        itemsArray = [Item]()
        arborescence = [String]()
        currentRootItemId = ApplicationManager.sharedInstance.user.rootItem.id
        firstFetch = true
        fetchItem(ApplicationManager.sharedInstance.user.rootItem.id, back: false, reloadFolder: false)
        indicator.strokeColor = UIColor.whiteColor()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeViewController.updateCell), name: kDownloadFinishNotificationKey, object: nil)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = UIColor.clearColor()
        self.tableView.separatorStyle = .None
        showBackButton(false)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : - Request method
    
    /**
     Create a folder
     @param folderName : a folder name
     @param folderId : a folder id
     */
    
    func createFolder(folderName : String, folderId : String) {
        ApplicationManager.sharedInstance.requester.createFolder(folderName, folderId: folderId, handler: {
            self.fetchItem(self.currentRootItemId, back: false, reloadFolder: true)
        }) { (code) in
            print(code)
            self.showErrorAlert("Création impossible", subtitle: "Impossible de créer le dossier. Erreur \(code)", buttonTitle: "Ok")
        }
    }
    
    /**
     Send picture to the server
     @param picture : a picture
     @param fileName : a file name
     */
    
    func sendPicture(picture : UIImage, fileName : String) {
        self.showIndicator(true)
        self.indicator.progress = 0
        ApplicationManager.sharedInstance.requester.uploadFile(picture, folderId: currentRootItemId, fileName : fileName, handler: {
            print("success")
            self.showSuccesAlert("Téléchargement réussi", subtitle: "Téléchargement de votre fichier réussi.")
            let returnElt = self.arborescenceIsOnRootItem()
            self.fetchItem(returnElt.0, back: returnElt.1, reloadFolder: false)
            }, progress: { (write, totalWrite, totalExpected) in
                dispatch_async(dispatch_get_main_queue(), {
                    print("Write : \(write)")
                    print("total Write : \(totalWrite)")
                    print("total expected : \(totalExpected)")
                    self.indicator.progress = self.percentOf(Float(totalWrite), minValue: 0, maxValue: Float(totalExpected))
                })
            }, fail: {(code) in
                self.showErrorAlert("Upload impossible", subtitle: "Impossible d'uploader le fichier. Erreur \(code)", buttonTitle: "Ok")
                self.hideIndicator()
                print("fail")
        })
    }
    
    /**
     Fetch item from the server
     @param itemIdentifier : the current folder id
     @param back : if action is a back to parent folder
     @param reloadFolder : if the current folder have to be reload
     */
    
    func fetchItem(itemIdentifier : String, back : Bool, reloadFolder : Bool) {
        showIndicator(false)
        ApplicationManager.sharedInstance.requester.fetchItems(itemIdentifier, handler: { (itemsList) in
            self.hideIndicator()
            self.itemsArray = itemsList
            self.reloadTableViewData()
            if (reloadFolder != true) {
                let show = self.firstFetch == true ? true : back
                self.showBackButton(!show)
                self.firstFetch = false
                if (back == true) {
                    self.popLastItemInArborescence()
                }
            }
        }) { (errorCode) in
            self.hideIndicator()
            self.showErrorAlert("Erreur", subtitle: "Impossible de récupérer les éléments. Erreur \(errorCode)", buttonTitle: "Ok")
        }
    }
    
    /**
     Download an item file at index path
     @param indexPath : an index path
     */
    
    func downloadItem(indexPath : NSIndexPath) {
        let item = itemsArray[indexPath.row]
        self.showIndicator(true)
        self.indicator.progress = 0
        ApplicationManager.sharedInstance.requester.downloadItems(item, progress: { (progress) in
            self.indicator.progress = self.percentOf(Float(progress.1), minValue: 0, maxValue: Float(progress.2))
            }, handler: {
                self.hideIndicator()
                self.fetchItem(self.currentRootItemId, back: false, reloadFolder: true)
                self.showSuccesAlert("Téléchargement", subtitle: "Téléchargement réussi !")
        }) { (code) in
            if (code == 516) {
                self.showErrorAlert("Téléchargement", subtitle: "Impossible de télécharger le fichier car vous l'avez déjà téléchargé.", buttonTitle: "Ok")
                self.hideIndicator()
            }
        }
    }
    
    // MARK: - UITableView delegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView.hidden = false
        self.emptyTableViewInfo.hidden = true
        if (itemsArray.count == 0) {
            self.tableView.hidden = true
            self.emptyTableViewInfo.hidden = false
        }
        return itemsArray.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(IdentifierItemCell) as! ItemsCustomTableViewCell
        
        let item = itemsArray[indexPath.row]
        cell.backgroundColor = UIColor.clearColor()
        cell.setItem(item)
        cell.selectionStyle = .None
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let item = itemsArray[indexPath.row]
        if (item.isDir == true) {
            currentRootItemId = item.id
            arborescence.append(item.parentId)
            self.title = item.name
            fetchItem(item.id, back: false, reloadFolder: false)
        }
        else {
            let informationAlert = SCLAlertView()
            if (ApplicationManager.sharedInstance.requester.fileExistAthPath(item.id+"."+item.extensionType) == true) {
                informationAlert.addButton("Ouvrir", action: {
                    self.openFile(item)
                    informationAlert.hideView()
                })
            }
            else {
                informationAlert.addButton("Télécharger", action: {
                    if (item.itemFileType != nil) {
                        self.downloadItem(indexPath)
                    }
                    else {
                        self.showErrorAlert("Ouverture impossible", subtitle: "Ce type de fichier sera bientôt lisible dans l'application.", buttonTitle: "Ok")
                    }
                })
            }
            informationAlert.addButton("Supprimer") {
                self.deleteItem(indexPath)
            }
            informationAlert.showCloseButton = false
            informationAlert.addButton("Fermer") {
                informationAlert.hideView()
            }
            informationAlert.showInfo("Information", subTitle: "identifiant : \(item.id) \n\n Nom : \(item.name) \n\n Content Type : \(item.contentType) \n\n Size : \(item.size) ko \n\n Date de modification : \(self.getDateStringFromTimeStamp(item.modificationDate))")
        }
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            //SCLAlertView().showWarning("Hello Warning", subTitle: "This is a more descriptive warning text.")
            let warningAlert = SCLAlertView()
            warningAlert.addButton("Supprimer") {
                self.deleteItem(indexPath)
            }
            warningAlert.showCloseButton = false
            warningAlert.addButton("Annuler") {
                warningAlert.hideView()
            }
            warningAlert.showWarning("Suppression", subTitle: "Souhaiter vous vraiment supprimer cet élément ?")
        }
    }
    
    // MARK : - Helper method
    
    /**
     Update a specific cell
     @param notification : a notification that contain the cell index that have to be update
     */
    
    func updateCell(notification : NSNotification) {
        let dict: Dictionary = notification.userInfo as! [String:String]
        let index = dict["index"]
        let indexPath = NSIndexPath(forRow: Int(index!)!, inSection: 0)
        if (isRowVisible(indexPath.row) == true) {
            self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }
    
    /**
     Check if a row is visible
     @param row : a row
     @return Bool : a boolean value that indicate if the row is visbile or not
     */
    
    func isRowVisible(row : Int) -> Bool {
        let indexes = self.tableView.indexPathsForVisibleRows
        for index in indexes! {
            if (index.row == row) {
                return true;
            }
        }
        return false
    }
    
    /**
     Reload the tableview
     */
    
    func reloadTableViewData() {
        self.tableView.reloadData()
    }
    
    /**
     Get a date string from timestamp
     @param timeStamp : a timestam
     */
    
    func getDateStringFromTimeStamp(timeStamp : String) -> String {
        let range = timeStamp.startIndex.advancedBy(10)
        let realTimeStamp = timeStamp.substringToIndex(range)
        let date = NSDate(timeIntervalSince1970: Double(realTimeStamp)!)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss Z"
        let dateStr = dateFormatter.stringFromDate(date)
        return dateStr
    }
    
    /**
     Delete an item at a path
     @param indexPath : an indexPath
     */
    
    func deleteItem(indexPath : NSIndexPath) {
        let item = self.itemsArray[indexPath.row]
        showIndicator(false)
        ApplicationManager.sharedInstance.requester.deleteItem(item.id, handler: {
            self.hideIndicator()
            self.itemsArray.removeAtIndex(indexPath.row)
            self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            }, fail: { (errorCode) in
                self.hideIndicator()
                self.showErrorAlert("Suppression impossible", subtitle: "Impossible de supprimer l'élément. Erreur \(errorCode)", buttonTitle: "Ok")
        })
    }
    
    /**
     Open a file from item
     @param item : an item
     */
    
    func openFile(item : Item) {
        switch item.itemFileType!  {
        case FileType.Image :
            self.performSegueWithIdentifier("ShowImage", sender: item)
        case FileType.Text :
            self.performSegueWithIdentifier("ShowText", sender: item)
        case FileType.PDF, FileType.HTML:
            self.performSegueWithIdentifier("ShowWebView", sender: item)
        case FileType.Video:
            playVideo(item.filePath)
        case FileType.Audio:
            playSound(item.filePath)
        }
    }
    
    /**
     Play a video with a path
     @param item : a path
     */
    
    func playVideo(path : String) {
        
        videoPlayer = AVPlayer(URL: NSURL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.player = videoPlayer
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        }
        catch _ {
            print("error")
        }
        
        self.presentViewController(playerController, animated: true) {
            self.videoPlayer.play()
        }
    }
    
    /**
     Play a sound with a path
     @param item : a path
     */
    
    func playSound(path : String)
    {
        do { Audioplayer = try AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: path), fileTypeHint: nil) }
        catch let error as NSError { print(error.description) }
        Audioplayer.numberOfLoops = 0
        Audioplayer.prepareToPlay()
        Audioplayer.play()
    }
    
    /**
     Open the picker controller with type
     @param type : the type of the picker (camera, library)
     */
    
    func openPicker(type : UIImagePickerControllerSourceType) {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = type
        self.presentViewController(myPickerController, animated: true, completion: nil)
    }
    
    /**
     Check if text contains blank space
     @param name : a file text
     @return Bool : a boolean value that indicate if the text contains blank space
     */
    
    func textContainsBlankSpace(name : String) -> (Bool) {
        let whitespaceSet = NSCharacterSet.whitespaceCharacterSet()
        if name.stringByTrimmingCharactersInSet(whitespaceSet) != "" {
            return false
        }
        return true
    }
    
    /**
     Show the back button or not
     @param show : a boolean value that indicate if the back button must be display
     */
    
    func showBackButton(show : Bool) {
        back.tintColor = show == true ? UIColor.whiteColor() : UIColor.clearColor()
    }
    
    /**
     Tell if the back button is hidden
     @return Bool : a boolean value that indicate if the back button is hidden or not
     */
    
    func backButtonIsHidden() -> Bool {
        let hidden = back.tintColor == UIColor.clearColor() ? true : false
        return hidden
    }
    
    /**
     Check if the arborescence on the root item
     @return (String, Bool) : a tuple that contains a folder identifier and a boolean value that indicate if it is the root folder
     */
    
    func arborescenceIsOnRootItem() -> (String, Bool) {
        if arborescence.count == 0 {
            return (currentRootItemId, true)
        }
        return (currentRootItemId, false)
    }
    
    // MARK : - Prepare for segue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier != "ShowAccount") {
            let detailViewController = segue.destinationViewController as! ReaderViewController
            let item = sender as! Item
            detailViewController.filePath = item.filePath
            detailViewController.fileName = item.name
        }
    }
    
    /**
     Remove the last item in the arborescence list
     */
    
    func popLastItemInArborescence() {
        arborescence.popLast()
    }
    
    /**
     return the percentage value of the progress for the activity indicator
     @param value : a value
     @param minValue : a minimum value
     @param maxValue : a maximum value
     @return Float : a percentage
     */
    
    func percentOf(value : Float, minValue : Float, maxValue : Float) -> Float {
        let range = maxValue - minValue
        let percent = (value - minValue) / range
        return percent
    }
    
    // MARK : - Alert View
    
    /**
     Uplaod alert
     */
    
    @IBAction func uplaodFile() {
        let informationAlert = SCLAlertView()
        informationAlert.addButton("Upload", action: {
            self.showUploadAlert()
            informationAlert.hideView()
        })
        informationAlert.addButton("Créer un dossier", action: {
            let createFolder = SCLAlertView()
            let txt = createFolder.addTextField("Nom du dossier")
            createFolder.addButton("Créer") {
                print("Text value: \(txt.text)")
                if (self.textContainsBlankSpace(txt.text!) == true || txt.text == nil) {
                    self.showInfoAlert("Upload Impossible", subtitle: "Le dossier doit avoir un nom.")
                }
                else {
                    self.createFolder(txt.text!, folderId: self.currentRootItemId)
                    createFolder.hideView()
                }
                
            }
            createFolder.addButton("Fermer") {
                createFolder.hideView()
            }
            createFolder.showCloseButton = false
            createFolder.showEdit("Création dossier", subTitle: "Entrer le nom du dossier")
        })
        informationAlert.showCloseButton = false
        informationAlert.addButton("Fermer") {
            informationAlert.hideView()
        }
        informationAlert.showInfo("Information", subTitle: "Pour envoyer un fichier, vous devez vous positionner dans le dossier de destination ou créer un dossier.")
    }
    
    /**
     Show error alert
     @param title : a title
     @param subtitle : a subtitle
     @param buttonTitle : a button title
     */
    
    func showErrorAlert(title : String, subtitle : String, buttonTitle : String) {
        let errorAlert = SCLAlertView()
        errorAlert.showCloseButton = false
        errorAlert.addButton(buttonTitle) {
            errorAlert.hideView()
        }
        errorAlert.showError(title, subTitle: subtitle)
    }
    
    /**
     Show success alert
     @param title : a title
     @param subtitle : a subtitle
     */
    
    func showSuccesAlert(title : String, subtitle : String) {
        let infoAlert = SCLAlertView()
        infoAlert.showCloseButton = false
        infoAlert.addButton("Ok") {
            infoAlert.hideView()
        }
        infoAlert.showSuccess(title, subTitle: subtitle)
    }
    
    /**
     Uplaod alert
     */
    
    func showUploadAlert() {
        let uploadAlert = SCLAlertView()
        uploadAlert.addButton("Sélectionner un fichier", action: {
            self.openPicker(.PhotoLibrary)
            uploadAlert.hideView()
        })
        uploadAlert.addButton("Ouvrir la caméra", action: {
            self.openPicker(.Camera)
            uploadAlert.hideView()
        })
        uploadAlert.showCloseButton = false
        uploadAlert.addButton("Fermer") {
            uploadAlert.hideView()
        }
        uploadAlert.showInfo("Upload", subTitle: "Vous pouvez directement prendre une photo ou sélectionner un fichier dans la galerie.")
    }
    
    /**
     Show info alert
     @param title : a title
     @param subtitle : a subtitle
     */
    
    func showInfoAlert(title : String, subtitle : String) {
        let uploadAlert = SCLAlertView()
        uploadAlert.addButton("J'ai compris", action: {
            uploadAlert.hideView()
        })
        uploadAlert.showCloseButton = false
        uploadAlert.addButton("Fermer") {
            uploadAlert.hideView()
        }
        uploadAlert.showInfo(title, subTitle: subtitle)
    }
    
    // MARK : - Acton
    
    /**
    Back to parent controller
     */
    
    @IBAction func backToParentFolder() {
        if (backButtonIsHidden() == false) {
            let identifier = arborescence.last
            
            if (identifier == ApplicationManager.sharedInstance.user.rootItem.id) {
                currentRootItemId = identifier
                fetchItem(arborescence.last!, back: true, reloadFolder: false)
                self.title = "OodriveTest"
            }
            else {
                self.popLastItemInArborescence()
                currentRootItemId = identifier
                fetchItem(currentRootItemId, back: false, reloadFolder: false)
            }
        }
    }
    
    /**
     Open the account controller
     */
    
    @IBAction func showAccount() {
        self.performSegueWithIdentifier("ShowAccount", sender: self)
    }
    
    // MARK : - Picker controller delegate
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        var fileExtension = ""
        if let referenceUrl = info[UIImagePickerControllerReferenceURL] as? NSURL {
            let urlString = referenceUrl.absoluteString
            let item = urlString.componentsSeparatedByString("ext=")
            fileExtension = item[1]
        }
        
        picker.dismissViewControllerAnimated(true) {
            let createFolder = SCLAlertView()
            let txt = createFolder.addTextField("Nom du fichier")
            createFolder.addButton("Valider") {
                print("Text value: \(txt.text)")
                if (self.textContainsBlankSpace(txt.text!) == true || txt.text == nil) {
                    self.showInfoAlert("Upload Impossible", subtitle: "Le fichier doit avoir un nom et ne pas contenir.")
                }
                else {
                    print(fileExtension)
                    let fileName = (txt.text?.lowercaseString)!+"."+fileExtension.lowercaseString
                    self.performSelector(#selector(HomeViewController.sendPicture(_:fileName:)), withObject: image, withObject: fileName)
                    createFolder.hideView()
                }
            }
            createFolder.addButton("Fermer") {
                createFolder.hideView()
            }
            createFolder.showCloseButton = false
            createFolder.showEdit("Nom du fichier", subTitle: "Entrer le nom du fichier")
        }
    }
    
    // MARK : - Activity management
    
    /**
     Hide an activity indicator
     */
    
    func hideIndicator() {
        maskView.hidden = true
        indicator.completeLoading(true)
        indicator.alpha = 1
        UIView.animateWithDuration(1) {
            self.indicator.alpha = 0
        }
    }
    
    /**
     Start an activity indicator with a progress
     */
    
    func showIndicator(progress : Bool) {
        maskView.hidden = false
        indicator.alpha = 0
        UIView.animateWithDuration(1) {
            self.indicator.alpha = 1
        }
        if (progress != true) {
            indicator.startLoading()
        }
    }
}


/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */


