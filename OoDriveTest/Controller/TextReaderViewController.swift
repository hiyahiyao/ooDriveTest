//
//  TextReaderViewController.swift
//  OoDriveTest
//
//  Created by Pierre Yao on 24/04/2016.
//  Copyright © 2016 Pierre Yao. All rights reserved.
//

import UIKit

class TextReaderViewController: ReaderViewController {
    
    @IBOutlet var textview : UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = fileName
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        do {
            self.textview.text = try String(contentsOfFile: self.filePath)
        }
        catch {
            
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
