//
//  LoginViewController.swift
//  OoDriveTest
//
//  Created by Pierre Yao on 19/04/2016.
//  Copyright © 2016 Pierre Yao. All rights reserved.
//

import UIKit
import Alamofire

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var login : UITextField!
    @IBOutlet var password : UITextField!
    @IBOutlet var indicator : activityIndicator!
    @IBOutlet var maskView : UIView!
    
    var keyboardIsShown : Bool!

    override func viewDidLoad() {
        super.viewDidLoad()

        login.delegate = self
        login.layer.borderWidth = 2.0
        login.layer.borderColor = UIColor.whiteColor().CGColor
        password.delegate = self
        password.layer.borderWidth = 2.0
        password.layer.borderColor = UIColor.whiteColor().CGColor
        
        maskView.hidden = true
        indicator.strokeColor = UIColor.whiteColor()
        
        keyboardIsShown = false
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Texfield delegate
    
    /**
     Login the user
     */
    
    @IBAction func login(sender : UIButton) {
        if login.text == kUserName && password.text == kUserPassword {
            showIndicator()
            
            ApplicationManager.sharedInstance.requester.loginUser({ (user) in
                self.hideIndicator()
                ApplicationManager.sharedInstance.user = user
                self.performSegueWithIdentifier("Login", sender: self)
                }, fail: { (errorCode) in
                    self.hideIndicator()
                    let alertView = SCLAlertView()
                    alertView.showCloseButton = false
                    alertView.addButton("Ok") {
                        alertView.hideView()
                    }
                    alertView.showError("Connexion impossible", subTitle: "Impossible de se connecter, veuillez reéssayer. Erreur \(errorCode)")
            })
        }
    }
    
    // MARK: - Texfield delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        return true
    }
    
    // MARK: - keyboard maganement
  
    func textFieldDidBeginEditing(textField: UITextField) {
        animateViewMoving(true, moveValue: 100)
    }
    func textFieldDidEndEditing(textField: UITextField) {
        animateViewMoving(false, moveValue: 100)
    }
    
    /**
     Move the view to display the keyboard without hidden textfield
     @param up : a boolean value that indicate if the view have to move to the top/bottom
     @param moveValue : value of the move
     */
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }
    
    // MARK: - Indicator management
    
    /**
     Start an activity indicator
     */
    
    func hideIndicator() {
        maskView.hidden = true
        indicator.completeLoading(true)
        indicator.alpha = 1
        UIView.animateWithDuration(0.2) {
            self.indicator.alpha = 0
        }
    }
    
    /**
     Start an activity indicator
     */
    
    func showIndicator() {
        indicator.alpha = 0
        UIView.animateWithDuration(0.2) {
            self.indicator.alpha = 1
        }
        maskView.hidden = false
        indicator.startLoading()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
