//
//  ViewController.swift
//  OoDriveTest
//
//  Created by Pierre Yao on 19/04/2016.
//  Copyright © 2016 Pierre Yao. All rights reserved.
//

import UIKit

@IBDesignable
class SquareButton: UIButton {
    
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    @IBOutlet var widhtConstraint: NSLayoutConstraint!
    
    @IBInspectable var borderWidth: CGFloat = 2.0
    @IBInspectable var cornerRadius: CGFloat = 0
    
    override var highlighted: Bool {
        willSet(willBeHighlighted) {
            super.highlighted = willBeHighlighted
            layer.backgroundColor = willBeHighlighted ? generalRedColor.CGColor : UIColor.whiteColor().CGColor
            layer.borderColor = willBeHighlighted ? generalRedColor.CGColor : titleColorForState(.Normal)?.CGColor
        }
    }

    override var selected: Bool {
        willSet(willBeSelected) {
            super.selected = willBeSelected
            layer.borderColor = willBeSelected ? UIColor.whiteColor().CGColor : titleColorForState(.Normal)?.CGColor
        }
    }

    override internal init(frame: CGRect) {
        super.init(frame: frame)
        
        setupButton()
    }
    
    required internal init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }
    
    /**
     Setup the custom button
     */
    
    func setupButton() {
        setTitleColor(generalRedColor, forState: .Normal)
        setTitleColor(UIColor.grayColor(), forState: .Disabled)
        setTitleColor(UIColor.whiteColor(), forState: .Highlighted)
        titleLabel?.font = UIFont.boldSystemFontOfSize(15)
        
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        layer.borderColor = self.titleColorForState(.Normal)?.CGColor
        layer.backgroundColor = UIColor.whiteColor().CGColor
        if let originalTitle = titleForState(.Normal) {
            let key = originalTitle
            let title = NSLocalizedString(key, comment:"")
            if title != key {
                setTitle(title, forState: .Normal)
            } else {
                setTitle(NSLocalizedString(originalTitle, comment:""), forState: .Normal)
            }
        }
    }

    override func prepareForInterfaceBuilder() {
        setupButton()
    }
    
    func setMutiplelineModeEnabled(multipleLineMode: Bool) {
        self.titleLabel?.numberOfLines = multipleLineMode ? 0 : 1
        self.titleLabel?.textAlignment = .Center
        setNeedsLayout()
    }
}
