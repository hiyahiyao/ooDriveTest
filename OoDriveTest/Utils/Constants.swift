//
//  Constants.swift
//  OoDriveTest
//
//  Created by Pierre Yao on 21/04/2016.
//  Copyright © 2016 Pierre Yao. All rights reserved.
//

import Foundation

//FIXME: Replace url parameter by http://your.ip:8080 or a server adress
let url = "http://192.168.1.87:8080"

// Login informations

let kUserName = "noel"
let kUserPassword = "foobar"

// Json parameter constants

let kFirstName = "firstName"
let kLastName = "lastName"
let kRootItem = "rootItem"
let kId = "id"
let kParentId = "parentId"
let kIsDir = "isDir"
let kName = "name"
let kSize = "size"
let kContentType = "contentType"
let kModificationDate = "modificationDate"

// Notification constant

let kDownloadFinishNotificationKey = "com.pierreyao.downloadFinishKey"

// Data format type

let rawTypes = [
    "text/plain": ["txt","text","conf","def","list","log","in","ini"],
    "text/html": ["html", "htm"],
    "text/css": ["css"],
    "text/csv": ["csv"],
    "text/xml": [],
    "text/javascript": [],
    "text/markdown": [],
    "text/x-markdown": ["markdown","md","mkd"],
    
    "audio/mpeg": ["mp3"],
    
    "video/mp4": ["mp4"],
    
    "image/bmp": ["bmp"],
    "image/png": ["png"],
    "image/gif": ["gif"],
    "image/jpeg": ["jpeg","jpg","jpe"],
    "image/svg+xml": ["svg","svgz"]
]