//
//  Item.swift
//  OoDriveTest
//
//  Created by Pierre Yao on 21/04/2016.
//  Copyright © 2016 Pierre Yao. All rights reserved.
//

import UIKit

enum FileType {
    case Image
    case PDF
    case Text
    case HTML
    case Audio
    case Video
}

class Item: NSObject {
    
    var id : String!
    var parentId : String!
    var isDir : Bool!
    var isRoot : Bool!
    var name : String!
    var size : String!
    var contentType : String!
    var extensionType : String!
    var modificationDate : String!
    var filePath : String!
    var isAlreadyDownload : Bool!
    var currentIndex : Int = 0
    var itemFileType : FileType!
    
    init(anId : String, aParentId : String, aIsDir : Bool, aIsRoot : Bool, aName : String, aSize : String, acontentType : String, anExtensionType : String, aModificationDate : String) {
        id = anId
        parentId = aParentId
        isDir = aIsDir
        isRoot = aIsRoot
        name = aName
        size = aSize
        contentType = acontentType
        extensionType = anExtensionType
        modificationDate = aModificationDate
        
        let localType = acontentType.componentsSeparatedByString("/")
        if (localType[0] != "Folder") {
            switch localType[0]  {
            case "image" :
                itemFileType = FileType.Image
            case "text" :
                itemFileType = FileType.Text
            case "video" :
                itemFileType = FileType.Video
            case "audio" :
                itemFileType = FileType.Audio
            default :
                break
            }
            
            switch localType[1] {
            case "pdf" :
                itemFileType = FileType.PDF
            case "html":
                itemFileType = FileType.HTML
            default :
                break
            }
        }
    }
    
    init(anId : String, aIsRoot : Bool, aIsDir : Bool, aName : String, aModificationDate : String) {
        id = anId
        isDir = aIsDir
        isRoot = aIsRoot
        name = aName
        modificationDate = aModificationDate
    }

}
