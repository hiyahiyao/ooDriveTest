//
//  ApplicationManager.swift
//  OoDriveTest
//
//  Created by Pierre Yao on 19/04/2016.
//  Copyright © 2016 Pierre Yao. All rights reserved.
//

import UIKit

// Color constant

let generalBlueColor = UIColor(rgba: "#4BB4E6")
let generalRedColor = UIColor(rgba: "#BE172E")

class ApplicationManager: NSObject {

    static let sharedInstance = ApplicationManager()
    let requester = Requester()
    var user = User()
    
}
