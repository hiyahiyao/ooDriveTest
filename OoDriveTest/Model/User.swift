//
//  User.swift
//  OoDriveTest
//
//  Created by Pierre Yao on 21/04/2016.
//  Copyright © 2016 Pierre Yao. All rights reserved.
//

import UIKit

class User: NSObject {
    
    var firstName : String!
    var lastName : String!
    var rootItem : Item!
    
    init(aFirstName : String, aLastName : String, aRootItem : Item) {
        firstName = aFirstName
        lastName = aLastName
        rootItem = aRootItem
    }
    
    override init() {
        super.init()
    }
}
