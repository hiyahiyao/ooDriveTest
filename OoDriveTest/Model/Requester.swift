//
//  Requester.swift
//  OoDriveTest
//
//  Created by Pierre Yao on 19/04/2016.
//  Copyright © 2016 Pierre Yao. All rights reserved.
//

import UIKit
import Alamofire

class Requester: NSObject {
    
    // MARK : - Request method
    
    /**
     Login an user
     @param handler : callback that return an User object
     @param fail : callback that return the error code
     */
    
    func loginUser(handler : (User) -> (), fail : (Int) -> ()) {
        let path = "/me"
        let requestUrl = url+path
        var user = User()
        
        Alamofire.request(.GET, requestUrl)
            .authenticate(user: kUserName, password: kUserPassword)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let resultValue = response.result.value {
                        let json = JSON(resultValue)
                        print("Json : \(json)")
                        let rootItem = Item(anId: json[kRootItem][kId].stringValue, aIsRoot: true, aIsDir: json[kRootItem][kIsDir].boolValue, aName: json[kRootItem][kName].stringValue, aModificationDate: json[kRootItem][kModificationDate].stringValue)
                        user = User(aFirstName: json[kFirstName].stringValue, aLastName: json[kLastName].stringValue, aRootItem: rootItem)
                        dispatch_async(dispatch_get_main_queue(), {
                            handler(user)
                        })
                    }
                case .Failure( _):
                    dispatch_async(dispatch_get_main_queue(), {
                        fail(response.result.error!.code)
                    })
                }
        }
    }
    
    /**
     Download an item from the server
     @param item : the item to be donwload
     @param progress : callback that return bytes write, total bytes write and expected total bytes
     @param handler : callback for success download
     @param fail : callback that return the error code
     */
    
    func downloadItems(item : Item, progress : (Int64, Int64, Int64) -> (), handler : () -> (), fail : (Int) -> ()) {
        let path = "/items"
        let itemId = "/\(item.id)"
        let data = "/data"
        let requestUrl = url+path+itemId+data
        let destination = Alamofire.Request.suggestedDownloadDestination(directory: .DocumentDirectory, domain: .UserDomainMask)
        Alamofire.download(.GET, requestUrl, destination: destination)
            .progress { bytesRead, totalBytesRead, totalBytesExpectedToRead in
                dispatch_async(dispatch_get_main_queue()) {
                    progress(bytesRead, totalBytesRead, totalBytesExpectedToRead)
                    print("Total bytes read on main queue: \(totalBytesRead)")
                }
            }
            .response { _, _, _, error in
                if let error = error {
                    dispatch_async(dispatch_get_main_queue(), {
                        fail(error.code)
                    })
                } else {
                    self.listFile()
                    dispatch_async(dispatch_get_main_queue(), {
                        item.filePath = self.renameFileWithNewName(item)
                        handler()
                    })
                    self.listFile()
                }
        }
    }
    
    /**
     Uplaod a file to the server
     @param image : image to send
     @param folderID : folder where file must be upload
     @param handler : callback for success download
     @param progress : callback that return bytes write, total bytes write and expected total bytes
     @param fail : callback that return the error code
     */
    
    func uploadFile(image : UIImage, folderId : String, fileName : String, handler : () -> (), progress : (Int64, Int64, Int64) -> (), fail : (Int) -> ()) {
        let path = "/items"
        let folder = "/\(folderId)"
        let requestUrl = url+path+folder
        var imageData : NSData = NSData()
        let nameComponents = fileName.componentsSeparatedByString(".")
        switch nameComponents[1] {
        case "jpg":
            imageData = UIImageJPEGRepresentation(image, 0.9)!
        case "png":
            imageData = UIImagePNGRepresentation(image)!
        default:
            imageData = UIImageJPEGRepresentation(image, 0.9)!
        }
        
        let upload = Alamofire.upload(.POST, requestUrl, headers: ["Content-Type": "application/octet-stream",
            "Content-Disposition": "attachment;filename*=utf-8''\(fileName)"], data: imageData)
        
        upload.progress { bytesWritten, totalBytesWritten, totalBytesExpectedToWrite in
            dispatch_async(dispatch_get_main_queue(), {
                progress(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite)
            })
        }
        upload.response { request, response, data, error in
            print(error)
            if let err = error {
                print(err)
                fail(err.code)
            }
            else {
                print(response)
                dispatch_async(dispatch_get_main_queue(), {
                    handler()
                })
            }
        }
    }
    
    /**
     Create a new folder
     @param folderName : name of the new folder
     @param folderID : folder where file must be upload
     @param handler : callback for success download
     @param fail : callback that return the error code
     */
    
    func createFolder(folderName : String, folderId : String, handler : () -> (), fail : (Int) -> ()) {
        let path = "/items"
        let folder = "/\(folderId)"
        let requestUrl = url+path+folder

        Alamofire.request(.POST, requestUrl, parameters: ["name":folderName], encoding: .JSON, headers: ["Content-Type": "application/json"])
            .responseJSON { response in
                dispatch_async(dispatch_get_main_queue(), { 
                    guard response.result.isSuccess else {
                        print("Error while fetching tags: \(response.result.error)")
                        fail(response.result.error!.code)
                        return
                    }
                    print(response)
                    handler()
                })
        }
    }
    
    /**
     Fetch items (Files/Folder) from the server
     @param itemIdentifier : the folder id
     @return Bool : boolean that indicate if the file exist or not
     @param handler : callback for success download that return array of item
     @param fail : callback that return the error code
     */
    
    func fetchItems(itemIdentifier : String, handler : ([Item]) -> (), fail : (Int) -> ()) {
        let path = "/items"
        let itemId = "/\(itemIdentifier)"
        let requestUrl = url+path+itemId
        var itemsArray = [Item]()
        
        Alamofire.request(.GET, requestUrl)
            .authenticate(user: kUserName, password: kUserPassword)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let resultValue = response.result.value {
                        let json = JSON(resultValue)
                        print("Json : \(json)")
                        var realIndex = 0
                        for index in 0..<json.count {
                            if (self.isHiddenFolder(json[index][kName].stringValue) == false) {
                                let item = Item(anId: json[index][kId].stringValue, aParentId: json[index][kParentId].stringValue, aIsDir: json[index][kIsDir].boolValue, aIsRoot: false, aName: json[index][kName].stringValue, aSize: json[index][kIsDir].boolValue == true ? "None" : json[index][kSize].stringValue, acontentType: json[index][kIsDir].boolValue == true ? "Folder" :
                                    json[index][kContentType].stringValue, anExtensionType : json[index][kIsDir].boolValue == true ? "Folder" : self.fileExtensionMatching(
                                        json[index][kName].stringValue, contentType: json[index][kContentType].stringValue), aModificationDate: json[index][kModificationDate].stringValue)
                                
                                if (item.isDir != true) {
                                    if (self.fileExistAthPath(item.id+"."+item.extensionType) == false) {
                                        if (item.itemFileType != nil) {
                                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                                                self.downloadItems(item, progress: { (read, totalRead, totalExpected) in
                                                    // maybe to display a progress
                                                    }, handler: {
                                                        dispatch_async(dispatch_get_main_queue()) {
                                                            NSNotificationCenter.defaultCenter().postNotificationName(kDownloadFinishNotificationKey, object: self, userInfo: ["itemID" : item.id, "index" : String(item.currentIndex)])
                                                            item.isAlreadyDownload = true
                                                        }
                                                    }, fail: { (code) in
                                                        // handler downloading failure
                                                })
                                            }
                                        }
                                        /*dispatch_after(delayTime, dispatch_get_main_queue()) {
                                            
                                        }*/
                                    }
                                    else {
                                        item.isAlreadyDownload = true
                                        item.filePath = self.getFileFullPath(item)
                                    }
                                }
                                item.currentIndex = realIndex
                                itemsArray.append(item)
                                realIndex+=1
                            }
                            else {
                                if (realIndex != 0) {
                                    realIndex-=1
                                }
                            }
                        }
                        handler(itemsArray)
                    }
                case .Failure( _):
                    fail(response.result.error!.code)
                }
        }
    }
    
    /**
     Delete an item from the server
     @param itemId : the item identifier
     @param handler : callback for success 
     @param fail : callback that return the error code
     */
    
    func deleteItem(itemId : String, handler : () ->(), fail : (Int) -> ()) {
        let path = "/items"
        let itemIdFormatted = "/"+itemId
        let requestUrl = url+path+itemIdFormatted
        
        Alamofire.request(.DELETE, requestUrl)
            .authenticate(user: kUserName, password: kUserPassword)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let resultValue = response.result.value {
                        let json = JSON(resultValue)
                        print("Json : \(json)")
                        dispatch_async(dispatch_get_main_queue(), {
                            handler()
                        })
                    }
                case .Failure( _):
                    dispatch_async(dispatch_get_main_queue(), {
                        fail(response.result.error!.code)
                    })
                }
        }
    }
    
    // MARK : - Helper method
    
    /**
     List all the file in a folder
     @return [String] : list of files
     */
    
    func listFile() -> [String] {
        let fileManager = NSFileManager.defaultManager()
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        var files : [String] = [""]
        do {
            let fileList = try fileManager.contentsOfDirectoryAtPath(documentDirectory)
            files = fileList
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
        return files
    }
    
    /**
     Check if a file exist at a path
     @param fileName : file name of the concern file
     @return Bool : boolean that indicate if the file exist or not
     */
    
    func fileExistAthPath(fileName : String) -> Bool {
        let paths = getBasePath().1 as NSString
        let getImagePath = paths.stringByAppendingPathComponent(fileName)
        let checkValidation = NSFileManager.defaultManager()
        
        if (checkValidation.fileExistsAtPath(getImagePath))
        {
            return true
        }
        return false
    }
    
    /**
     Create the right format for item path
     @param itemName : the name of the concern item
     @return String : the path
     */
    
    func formatUrlPath(itemName : String) -> String {
        let docUrl = getBasePath()
        let fileURL = docUrl.0.URLByAppendingPathComponent(itemName)
        return fileURL.path!
    }
    
    /**
     Get a file full path
     @param item : the concern item
     @return String : the full path
     */
    
    func getFileFullPath(item : Item) -> String {
        let filePath = formatUrlPath(item.id)+"."+item.extensionType
        return filePath
    }
    
    /**
     Check if a file extension match with extensions of RawType
     @param fileName : a file name
     @param contentType : a content type (that can be image/jpeg for example)
     @return String : an extension
     */
    
    func fileExtensionMatching(fileName : String, contentType : String) -> String {
        let extensions = fileName.componentsSeparatedByString(".")
        if let extensionsType = rawTypes[contentType] {
            for extensionName in extensionsType {
                if (extensions[1] == extensionName) {
                    return extensionName
                }
            }
        }
        return extensions[1]
    }
    
    /**
     Retrieve the basic destination of downloaded items
     @return (NSURL, String) : a tuple with a path as string and URL
     */
    
    func getBasePath() -> (NSURL, String) {
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        return (documentsURL, documentDirectory)
    }
    
    /**
     Rename a file with his identifer as name because all downloaded items are named "data.extension"
     @param item : an item
     @return String : a new name
     */
    
    func renameFileWithNewName(item : Item) -> String {
        var originalItem = ""
        var destinationItem = ""
        do {
            let type = item.contentType.componentsSeparatedByString("/")
            switch item.itemFileType!  {
            case FileType.Image :
                originalItem = formatFileName("/"+"data."+type[1])
                destinationItem = formatFileName("/"+item.id+"."+item.extensionType)
            case FileType.Text :
                originalItem = formatFileName("/"+"data."+item.extensionType)
                destinationItem = formatFileName("/"+item.id+"."+item.extensionType)
            case FileType.PDF :
                originalItem = formatFileName("/"+"data."+type[1])
                destinationItem = formatFileName("/"+item.id+"."+item.extensionType)
            case FileType.HTML :
                originalItem = formatFileName("/"+"data."+type[1])
                destinationItem = formatFileName("/"+item.id+"."+item.extensionType)
            case FileType.Audio :
                originalItem = formatFileName("/"+"data."+item.extensionType)
                destinationItem = formatFileName("/"+item.id+"."+item.extensionType)
            case FileType.Video :
                originalItem = formatFileName("/"+"data."+type[1])
                destinationItem = formatFileName("/"+item.id+"."+item.extensionType)
            }
            
            try NSFileManager.defaultManager().moveItemAtPath(originalItem, toPath: destinationItem)
            return destinationItem
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
        return destinationItem
    }
    
    /**
     Format a file name
     @param name : a name
     @return String : a path
     */
    
    func formatFileName(name : String) -> String {
        return getBasePath().1+name
    }
    
    /**
     Check if a folder is an hidden folder
     @param folderName : a folder name
     @return Bool : a boolean that say if a folder is hidden
     */
    
    func isHiddenFolder(folderName : String) -> Bool {
        if (folderName.characters.first == ".") {
            return true
        }
        return false
    }
}
